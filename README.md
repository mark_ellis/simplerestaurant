#Entity Framework setup

If the Entity framework connection string is in a different project then need to run the following:

dotnet ef dbcontext info -s ..\SimpleRestaurant\SimpleRestaurant.csproj

##Then need to do data migration:
Migraton create schema changes.

dotnet-ef migrations add initialcreate -s ..\SimpleRestaurant\SimpleRestaurant.csproj

##Add create database:

dotnet-ef database update -s ..\SimpleRestaurant\SimpleRestaurant.csproj


#Publishing

//Publish
dotnet publish -o c:\temp\Simplerestaurant

##run 
dotnet simplerestaurant.dll

won't have node_modules but could run npm install
Better way :
update proj file to run npm install post build and include node_modules directory

Include framework  (idependence from other .net installs)
dotnet publish -o c:\temp\Simplerestaurant -self-contained -r win-x64

##Linux
sudo apt install nginx-full  

wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-3.1


## nginx case in-sensitive path
server {
    listen        80;
    server_name   tycho-station;
    location ~* ^/restaurants{
        proxy_pass         http://localhost:5000;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
    }
}





