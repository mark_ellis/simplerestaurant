﻿using SimpleRestaurant.Core;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace SimpleRestaurant.Data
{
    public interface IRestaurantData
    {
        Restaurant GetById(int id);
        IEnumerable<Restaurant> GetAll();
        IEnumerable<Restaurant> GetRestaurantsByName(string name);
        Restaurant Update(Restaurant updatedRestaurant);
        Restaurant Add(Restaurant newRestaurant);
        Restaurant Delete(int id);
        int Commit();
        int GetCountOfRestaurants();
    }
}
