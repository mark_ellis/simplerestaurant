﻿using Microsoft.EntityFrameworkCore;
using SimpleRestaurant.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRestaurant.Data
{
    public class RestaurantContext : DbContext
    {

        public RestaurantContext(DbContextOptions<RestaurantContext> options)
            : base(options)
        {

        }
        public DbSet<Restaurant> Restaurants { get; set; }
    }
}
