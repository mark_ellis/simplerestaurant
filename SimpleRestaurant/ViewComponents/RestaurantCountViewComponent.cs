﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.ViewComponents
{
    public class RestaurantCountViewComponent 
        : ViewComponent
    {

        public RestaurantCountViewComponent(IRestaurantData restaurantData)
        {
            RestaurantData = restaurantData;
        }

        private IRestaurantData RestaurantData { get; }

        public IViewComponentResult Invoke()
        {
            var count = RestaurantData.GetCountOfRestaurants();
            return View(count);
        }
    }
}
