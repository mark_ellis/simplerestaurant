﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SimpleRestaurant.Core;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.Restaurants
{
    public class ListModel : PageModel
    {

        public string Message { get; set; }
        [BindProperty(SupportsGet =true)]
        public string SearchTerm { get; set; }
        public IConfiguration Config { get; }
        public IRestaurantData RestaurantData { get; }
        public ILogger<ListModel> Logger { get; }
        public IEnumerable<Restaurant> Restaurants { get; set; }

        public ListModel(IConfiguration config, 
            IRestaurantData restaurantData,
            ILogger<ListModel> logger)
        {
            Config = config;
            RestaurantData = restaurantData;
            Logger = logger;
        }
        public void OnGet()
        {
            Logger.LogInformation("Executring list model");
            Message = Config["Message"];
            Restaurants = RestaurantData.GetRestaurantsByName(SearchTerm);
        }
    }
}