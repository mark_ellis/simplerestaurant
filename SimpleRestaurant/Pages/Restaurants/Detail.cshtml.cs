﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SimpleRestaurant.Core;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.Restaurants
{
    public class DetailModel : PageModel
    {

        public DetailModel(IRestaurantData restaurantData)
        {
            RestaurantData = restaurantData;
        }

        public Restaurant Restaurant { get; set; }
        public IRestaurantData RestaurantData { get; }
        [TempData]
        public string Message { get; set; }

        public IActionResult OnGet(int restaurantID)
        {
            Restaurant = RestaurantData.GetById(restaurantID);
            if (Restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}