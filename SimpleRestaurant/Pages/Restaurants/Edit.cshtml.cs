﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.ProjectModel;
using SimpleRestaurant.Core;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.Restaurants
{
    public class EditModel : PageModel
    {

        public EditModel(IRestaurantData restaurantData, IHtmlHelper helper)
        {
            RestaurantData = restaurantData;
            Helper = helper;
        }

        public IRestaurantData RestaurantData { get; }
        public IHtmlHelper Helper { get; }
        [BindProperty]
        public Restaurant Restaurant { get; set; }
        public IEnumerable<SelectListItem> Cuisines { get; set; }

        public IActionResult OnGet(int? restaurantId)
        {
            Cuisines = Helper.GetEnumSelectList<CuisineType>();
            if (restaurantId.HasValue)
            {
                Restaurant = RestaurantData.GetById(restaurantId.Value);
            }
            else
            {
                Restaurant = new Restaurant();
            }

            if (Restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                Cuisines = Helper.GetEnumSelectList<CuisineType>();
                return Page();
            }
            if (Restaurant.id > 0) 
            {
                RestaurantData.Update(Restaurant);
            }
            else
            {
                RestaurantData.Add(Restaurant);
            }
            RestaurantData.Commit();
            TempData["Message"] = "Restaurant Saved";
            return RedirectToPage("./Detail", new { restaurantId = Restaurant.id });
        }
    }
}
