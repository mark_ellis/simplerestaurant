﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SimpleRestaurant.Core;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.R2
{
    public class IndexModel : PageModel
    {
        private readonly SimpleRestaurant.Data.RestaurantContext _context;

        public IndexModel(SimpleRestaurant.Data.RestaurantContext context)
        {
            _context = context;
        }

        public IList<Restaurant> Restaurant { get;set; }

        public async Task OnGetAsync()
        {
            Restaurant = await _context.Restaurants.ToListAsync();
        }
    }
}
