﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SimpleRestaurant.Core;
using SimpleRestaurant.Data;

namespace SimpleRestaurant.Pages.R2
{
    public class DetailsModel : PageModel
    {
        private readonly SimpleRestaurant.Data.RestaurantContext _context;

        public DetailsModel(SimpleRestaurant.Data.RestaurantContext context)
        {
            _context = context;
        }

        public Restaurant Restaurant { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Restaurant = await _context.Restaurants.FirstOrDefaultAsync(m => m.id == id);

            if (Restaurant == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
